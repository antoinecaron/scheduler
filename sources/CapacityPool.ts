import Bluebird from 'bluebird';

import os from 'os-utils';

type QueueItem<T> = {
	executor: () => Promise<T>;
	resolve: (res: T) => void;
	reject: (error: Error) => void;
}

export type CapacityPoolOptions = {
	concurrency?: number;
	cpu?: number; // per cent
	memory?: number; // Mo
	frequency?: {
		limit: number;
		period: number;
	};
}

export class CapacityPool {
	options: CapacityPoolOptions;
	concurrency = 0;
	frequency = 0;
	queue: QueueItem<any>[] = [];
	lockers: string[] = []; // TODO use Object {}
	subscriptions: NodeJS.Timeout[] = [];

	get isLocked(): boolean {
		return Boolean(this.lockers.length);
	}

	constructor(options: CapacityPoolOptions) {
		this.options = options;

		this.init();
	}

	init(): void {
		if (this.options.cpu) {
			const id = setInterval(() => os.cpuUsage((rate) => this.updateCPU(rate)), 1000);
			this.subscriptions.push(id);
		}

		if (this.options.memory) {
			const id = setInterval(() => this.updateMemory(os.freemem()), 1000);
			this.subscriptions.push(id);
		}
	}

	dispose() {
		this.subscriptions.forEach(clearInterval);
		this.subscriptions = [];
	}

	private updateConcurrency(amount = 1): void {
		this.concurrency += amount;
		if (this.options.concurrency) {
			this.setLocker('CONCURRENCY', this.concurrency >= this.options.concurrency);
		}
	}

	private updateFrequency(amount = 1): void {
		this.frequency += amount;
		if (this.options.frequency) {
			this.setLocker('FREQUENCY', this.frequency >= this.options.frequency.limit);
		}
	}

	private updateMemory(freeMemory: number): void {
		if (this.options.memory) {
			this.setLocker('MEMORY', freeMemory < this.options.memory);
		}
	}

	private updateCPU(cpuRate: number): void {
		if (this.options.cpu) {
			this.setLocker('CPU', 100 * cpuRate > this.options.cpu);
		}
	}

	private preFire(): void {
		this.updateConcurrency();
		this.updateFrequency();
	}

	private postFire(): void {
		this.updateConcurrency(-1);
		if (this.options.frequency && this.options.frequency.period) {
			setTimeout(() => this.updateFrequency(-1), this.options.frequency.period);
		}
	}

	private fire<T>({ executor, resolve, reject }: QueueItem<T>): void {
		this.preFire();
		setImmediate(async () => {
			try {
				resolve(await executor());
			} catch (error) {
				reject(error);
			}
			this.postFire();
		});
	}

	private dequeue(): void {
		if (this.queue.length && !this.isLocked) {
			this.fire(this.queue.shift()!);
		}
	}

	onNext<T>(executor: () => Promise<T>): Promise<T> {
		return new Bluebird<T>((resolve, reject) => {
			this.queue.push({ executor, resolve, reject });
			this.dequeue();
		});
	}

	setLocker(locker: string, set: boolean): void {
		const callee = set ? this.lock : this.unlock;
		callee.bind(this)(locker, true);
	}

	lock(locker: string, uniq = false): void {
		if (!uniq || this.lockers.indexOf(locker) === -1) {
			this.lockers.push(locker);
		}
	}

	unlock(locker: string, all = false): void {
		do {
			const index = this.lockers.indexOf(locker);
			if (index === -1) {
				break;
			}
			this.lockers.splice(index, 1);
		} while (all);
		this.dequeue();
	}
}
