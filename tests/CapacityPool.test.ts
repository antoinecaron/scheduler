import 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';

import Bluebird from 'bluebird';
import { iterate } from '@plokkke/toolbox';

import { CapacityPool } from '../sources/CapacityPool';

describe('CapacityPool', () => {
	describe('Contruct', () => {
		it('Construct', () => {
			const pool = new CapacityPool({});
			return expect(pool.isLocked).equal(false);
		});
	});
	describe('Locking', () => {
		it('Lock should lock', () => {
			const pool = new CapacityPool({});
			pool.lock('LOCKER');
			return expect(pool.isLocked).equal(true);
		});
		it('Unlock should unlock', () => {
			const pool = new CapacityPool({});
			pool.lock('LOCKER');
			pool.unlock('LOCKER');
			return expect(pool.isLocked).equal(false);
		});
		it('SetLocker', () => {
			const pool = new CapacityPool({});
			pool.setLocker('LOCKER', true);
			return expect(pool.isLocked).equal(true);
		});
		it('SetLocker', () => {
			const pool = new CapacityPool({});
			pool.setLocker('LOCKER', true);
			pool.setLocker('LOCKER', false);
			return expect(pool.isLocked).equal(false);
		});
		it('Multi lockers', () => {
			const pool = new CapacityPool({});
			pool.lock('LOCKER');
			pool.lock('LOCKER2');
			pool.unlock('LOCKER');
			return expect(pool.isLocked).equal(true);
		});
	});
	describe('On Next', () => {
		it('Should be executed', () => {
			const result = 42;
			const executor = sinon.stub().callsFake(() => Bluebird.resolve(result));
			const pool = new CapacityPool({});
			return pool.onNext(executor).then((res) => {
				expect(res).equal(result);
				expect(executor.called).equal(true);
			});
		});
		it('Should not be executed before unlock', () => {
			const result = 42;
			const executor = sinon.stub().callsFake(() => Bluebird.resolve(result));
			const pool = new CapacityPool({});
			pool.lock('LOCKER');
			setTimeout(() => pool.unlock('LOCKER'), 50);
			return pool.onNext(executor).then((res) => {
				expect(res).equal(result);
				expect(executor.called).equal(true);
			});
		});
		it('Should be called one by one', () => {
			let count = 0;
			const concurrency = 1;
			const executors = iterate(10).map(() => sinon.stub().callsFake(async () => {
				count += 1;
			}));
			const pool = new CapacityPool({ concurrency });
			return Bluebird.all(
				executors.map((executor) => pool.onNext(executor).then(() => {
					count -= 1;
					expect(count).lessThan(concurrency);
					expect(executor.called).equal(true);
				})),
			);
		});
		it('Should be called with max capacity', () => {
			let count = 0;
			const concurrency = 3;
			const executors = iterate(10).map(() => sinon.stub().callsFake(async () => {
				count += 1;
			}));
			const pool = new CapacityPool({ concurrency });
			return Bluebird.all(
				executors.map((executor) => pool.onNext(executor).then(() => {
					count -= 1;
					expect(count).lessThan(concurrency);
					expect(executor.called).equal(true);
				})),
			);
		});
	});
});
